package ru.tsc.chertkova.tm.repository;

import ru.tsc.chertkova.tm.api.repository.IUserOwnerRepository;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M>
        implements IUserOwnerRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(final String userId) {
        findAll(userId).clear();
    }

    @Override
    public List<M> findAll(final String userId) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public M findById(final String userId, final String id) {
        return models.stream()
                .filter(item -> id.equals(item.getId()) &&
                        userId.equals(item.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(final String userId) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .toArray().length;
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted((Comparator<? super M>) sort.getComparator())
                .collect(Collectors.toList());
    }

}
