package ru.tsc.chertkova.tm.repository;

import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.model.User;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return models.stream()
                .filter(item -> login.equals(item.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return models.stream()
                .filter(item -> email.equals(item.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
