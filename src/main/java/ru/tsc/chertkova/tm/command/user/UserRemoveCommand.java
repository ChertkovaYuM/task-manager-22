package ru.tsc.chertkova.tm.command.user;

import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    public static final String NAME = "user-remove";

    public static final String DESCRIPTION = "Remove user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
