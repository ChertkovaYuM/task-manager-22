package ru.tsc.chertkova.tm.command.user;

import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class UserRegistrationCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry-profile";

    public static final String DESCRIPTION = "Registration user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION PROFILE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        getUserService().create(login, password, email);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
