package ru.tsc.chertkova.tm.command.project;

import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class TaskUnbindToProjectCommand extends AbstractProjectCommand {

    public static final String NAME = "unbind-task-from-project";

    public static final String DESCRIPTION = "Unbind task from project.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }

}
