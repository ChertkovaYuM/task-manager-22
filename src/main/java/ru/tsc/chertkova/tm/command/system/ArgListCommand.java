package ru.tsc.chertkova.tm.command.system;

import ru.tsc.chertkova.tm.api.model.ICommand;
import ru.tsc.chertkova.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgListCommand extends AbstractSystemCommand {

    public static final String NAME = "arguments";

    public static final String DESCRIPTION = "Show arguments list.";

    public static final String ARGUMENT = "-arg";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand command : commands) {
            String argument = command.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(command.getArgument());
        }
    }

}
