package ru.tsc.chertkova.tm.command.project;

import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractProjectCommand {

    public static final String NAME = "bind-task-to-project";

    public static final String DESCRIPTION = "Bind task to project.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String userId = getUserId();
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}
