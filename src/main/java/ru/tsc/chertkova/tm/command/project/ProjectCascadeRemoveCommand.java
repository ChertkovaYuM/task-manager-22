package ru.tsc.chertkova.tm.command.project;

import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class ProjectCascadeRemoveCommand extends AbstractProjectCommand {

    public static final String NAME = "project-cascade-remove";

    public static final String DESCRIPTION = "Project cascade remove.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CASCADE REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, projectId);
    }

}
