package ru.tsc.chertkova.tm.command.project;

import ru.tsc.chertkova.tm.api.service.IProjectService;
import ru.tsc.chertkova.tm.api.service.IProjectTaskService;
import ru.tsc.chertkova.tm.command.AbstractCommand;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public String getArgument() {
        return null;
    }

    public void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

    public IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        final Status status = project.getStatus();
        if (status != null) System.out.println("STATUS: " + status.getDisplayName());
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
