package ru.tsc.chertkova.tm.command.user;

import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    public static final String NAME = "user-lock";

    public static final String DESCRIPTION = "Lock user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
