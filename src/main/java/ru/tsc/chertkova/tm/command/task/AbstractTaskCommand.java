package ru.tsc.chertkova.tm.command.task;

import ru.tsc.chertkova.tm.api.service.ITaskService;
import ru.tsc.chertkova.tm.command.AbstractCommand;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public String getArgument() {
        return null;
    }

    public void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    public ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    public void showTask(final Task task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        final Status status = task.getStatus();
        if (status != null) System.out.println("STATUS: " + status.getDisplayName());
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
