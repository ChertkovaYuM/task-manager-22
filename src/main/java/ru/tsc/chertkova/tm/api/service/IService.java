package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    M remove(M model);

    void clear();

    M findById(String id);

    M removeById(String id);

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    void removeAll(Collection<M> collection);

    int getSize();

    boolean existsById(String id);

}
