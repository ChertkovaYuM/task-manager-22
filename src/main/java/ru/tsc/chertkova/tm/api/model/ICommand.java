package ru.tsc.chertkova.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
