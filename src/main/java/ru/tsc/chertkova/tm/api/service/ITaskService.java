package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    Task create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateById(String userId, String id, String name, String description);

    Task changeTaskStatusById(String userId, String id, Status status);

}
