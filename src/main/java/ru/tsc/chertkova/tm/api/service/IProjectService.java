package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnerService<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    Project updateById(String userId, String id, String name, String description);

    Project changeProjectStatusById(String userId, String id, Status status);

}
